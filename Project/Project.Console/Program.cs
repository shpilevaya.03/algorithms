﻿using System;
using Project.Core;
using Project.Core.Algorithms;
using System.Numerics;
using Project.ConsoleUI;

namespace Project
{
    class Program
    {
        public static void Main()
        {
            Console.CursorVisible = false;
            MainMenu menu = new MainMenu();
            bool exit = false;

            //FileWriter writer = new FileWriter();
            //writer.GenerateFile(5000000);

            do
            {
                menu.Draw();

                ConsoleKeyInfo key = Console.ReadKey(true);

                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        menu.Next();
                        break;
                    case ConsoleKey.UpArrow:
                        menu.Prev();
                        break;
                    case ConsoleKey.Enter:
                        exit = menu.Select();
                        break;
                }
            }
            while (!exit);
        }
    }
}