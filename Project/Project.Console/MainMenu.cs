﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core.Algorithms;

namespace Project.ConsoleUI
{
    class MainMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Константный алгоритм", Algorithm = new ConstantAlgorithm(), IsSelected = true },
            new MenuItem() { Text = "Алгоритм суммы элементов", Algorithm = new SumAlgorithm() },
            new MenuItem() { Text = "Алгоритм произведения элементов", Algorithm = new MultiplicationAlgorithm() },
            new MenuItem() { Text = "Наивный алгоритм вычисления значения многочлена", Algorithm = new FindingPolynomialAlgorithm() },
            new MenuItem() { Text = "Алгоритм Горнера", Algorithm = new HornerAlgorithm() },
            new MenuItem() { Text = "Сортировка пузырьком", Algorithm = new BubbleSortAlgorithm() },
            new MenuItem() { Text = "Быстрая сортировка", Algorithm = new QuickSortAlgorithm() },
            new MenuItem() { Text = "Гибридный алгоритм сортировки", Algorithm = new TimsortAlgorithm() },
            new MenuItem() { Text = "Простой алгоритм возведения в степень", Algorithm = new SimplePower() },
            new MenuItem() { Text = "Рекурсивный алгоритм возведения в степень", Algorithm = new RecPower() },
            new MenuItem() { Text = "Первый быстрый алгоритм возведения в степень", Algorithm = new QuickPower() },
            new MenuItem() { Text = "Второй быстрый алгоритм возведения в степень", Algorithm = new QuickPower1() },

            new MenuItem() { Text = "Умножение матриц", Algorithm = new MatrixMultiplicationAlgorithm() },

            new MenuItem() { Text = "Рекурсивное нахождение наибольшего общего делителя", Algorithm = new GCDAlgorithm() },
            new MenuItem() { Text = "Нахождение уникальных чисел в массиве", Algorithm = new UniqueNumbersAlgorithm() },
            new MenuItem() { Text = "Алгоритм Карацубы", Algorithm = new KaratsubaAlgorithm() },

            new MenuItem() { Text = "Выход", Algorithm = null }
        };
    }
}
