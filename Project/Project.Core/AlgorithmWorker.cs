﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Diagnostics;

namespace Project
{
    public static class AlgorithmWorker
    {
        public static string path = "time.csv";
        public static string pathStep = "steps.csv";
        public static StringBuilder text = new StringBuilder();
        /// <summary>Выводит на консоль время выполнения алгоритма, возвращает количество секунд, потраченных на выполнение</summary>
        public static void MeasureExecutionTime<T1, T2>(T1 arr, Func<T1, T2> func)
        {
            long freq = Stopwatch.Frequency; //частота таймера
            Stopwatch stopwatch = new Stopwatch();
            var results = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                stopwatch.Reset();
                stopwatch.Start();
                var a = func(arr);
                stopwatch.Stop();
                double sec = (double)stopwatch.ElapsedTicks / freq; //переводим такты в секунды
                Console.WriteLine($"Время выполнения в тактах \t{stopwatch.ElapsedTicks}\r\nВремя в секундах \t\t{sec}\r\n");
                results.Add(sec);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее время выполнения алгоритма: {results.Sum() / results.Count()} с\r\n" +
                new string('-', 20) + "\r\n");

            text.Append(results.Sum() / results.Count());
        }

        public static void MeasureExecutionTime(int[,] a, int[,] b, Func<int[,], int[,], int[,]> func)
        {
            long freq = Stopwatch.Frequency; //частота таймера
            Stopwatch stopwatch = new Stopwatch();
            var results = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                stopwatch.Reset();
                stopwatch.Start();
                func(a, b);
                stopwatch.Stop();
                double sec = (double)stopwatch.ElapsedTicks / freq; //переводим такты в секунды
                Console.WriteLine($"Время выполнения в тактах \t{stopwatch.ElapsedTicks}\r\nВремя в секундах \t\t{sec}\r\n");
                results.Add(sec);
            }

            results.Remove(results.Max());

            Console.WriteLine($"Среднее время выполнения алгоритма: {results.Sum() / results.Count()} с\r\n" +
                new string('-', 20) + "\r\n");

            text.Append(results.Sum() / results.Count() + " ");
        }

        public static void RepeatMeasure<T1, T2>(List<T1> info, Func<List<T1>, T2> func, int count, int step)
        {
            text.Clear();

            for (int i = 0; i < step; i++)
            {
                var arr = info.GetRange(0, count * (i + 1)).ToArray();
                Console.WriteLine($"Количество элементов: {arr.Length}");
                text.Append(arr.Length + " ");
                MeasureExecutionTime(arr.ToList(), func);
                text.AppendLine();
                GC.Collect();
            }

            File.WriteAllText(path, text.ToString());
        }

        public static void CountSteps(Func<int, int, int> func)
        {
            text.Clear();

            for (int i = 0; i < 2000; i++)
            {
                var steps = func(2, i);
                text.Append(i + " " + steps + "\n");
                Console.WriteLine($"Количество шагов для возведения в {i}-ую степень: {steps}");
            }

            File.WriteAllText(pathStep, text.ToString());
        }
    }
}
