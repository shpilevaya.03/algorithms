﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class BubbleSortAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        List<int> Sort(List<int> arr)
        {
            int temp = 0;
            for (int write = 0; write < arr.Count; write++)
            {
                for (int sort = 0; sort < arr.Count - 1; sort++)
                {
                    if (arr[sort] > arr[sort + 1])
                    {
                        temp = arr[sort + 1];
                        arr[sort + 1] = arr[sort];
                        arr[sort] = temp;
                    }
                }
            }

            return arr;
        }

        public void Execute(FileReader fileReader)
        {
            var data = fileReader.ReadFile();
            AlgorithmWorker.RepeatMeasure(data, Sort, 100, 100);
        }
    }
}
