﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class MultiplicationAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        int Multiply(List<int> arr)
        {
            int multiply = 1;
            foreach (int i in arr)
            {
                multiply *= i;
            }
            return multiply;
        }

        public void Execute(FileReader fileReader)
        {
            var data = fileReader.ReadFile();
            AlgorithmWorker.RepeatMeasure(data, Multiply, 100000, 100);
        }
    }
}
