﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class HornerAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;
        static double Horner(List<int> a)
        {
            float x = 1.5f;
            int n = a.Count - 1;
            float y = a[n];

            for (int i = n - 1; i >= 0; i--)
            {
                y = x * y + a[i];
            }
            return y;
        }

        public void Execute(FileReader fileReader)
        {
            var arr = fileReader.ReadFile();

            AlgorithmWorker.RepeatMeasure(arr, Horner, 5000, 100);
        }
    }
}
