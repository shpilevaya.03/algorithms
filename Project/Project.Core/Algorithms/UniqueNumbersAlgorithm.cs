﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class UniqueNumbersAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        List<int> PrintTwoSingleNumbers(List<int> A)
        {
            Dictionary<int, int> map = new Dictionary<int, int>();
            List<int> results = new List<int>();

            int n = A.Count;
            for (int i = 0; i < n; i++)
            {
                if (map.ContainsKey(A[i]))
                    map.Remove(A[i]);
                else
                    map.Add(A[i], 1);
            }

            foreach (KeyValuePair<int, int> it in map)
            {
                if (it.Value == 1)
                {
                    results.Add(it.Key);
                }
            }
            return results;
        }

        public void Execute(FileReader fileReader)
        {
            var data = fileReader.ReadFile();
            AlgorithmWorker.RepeatMeasure(data, PrintTwoSingleNumbers, 10000, 100);
        }
    }
}