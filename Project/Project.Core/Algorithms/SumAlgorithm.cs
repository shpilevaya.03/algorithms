﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class SumAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        int Sum(List<int> arr)
        {
            int sum = 0;
            foreach (int i in arr)
            {
                sum += i;
            }
            return sum;
        }

        public void Execute(FileReader fileReader)
        {
            var data = fileReader.ReadFile();
            AlgorithmWorker.RepeatMeasure(data, Sum, 100000, 100);
        }
    }
}
