﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class FindingPolynomialAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        static double Straight(List<int> array)
        {
            double P = 0;
            double x = 1.5;

            for (int i = 0; i < array.Count; i++)
            {
                P += array[i] * Math.Pow(x, i);
            }
            return P;
        }

        public void Execute(FileReader fileReader)
        {
            var arr = fileReader.ReadFile();

            AlgorithmWorker.RepeatMeasure(arr, Straight, 5000, 100);
        }
    }
}
