﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class RecPower : IAlgorithm
    {
        public DataPath? AlgorithmPath => null;

        int RecPow(int num, int pow)
        {
            var res = 1;
            if (pow == 0)
                return res;

            var counter = 0;
            counter = RecPow(num, pow / 2);
            if (pow % 2 == 1)
            {
                res = res * res * num;
                counter += 2;
            }
            else
            {
                res = res * res;
                counter++;
            }

            return counter;
        }

        public void Execute(FileReader fileReader)
        {
            AlgorithmWorker.CountSteps(RecPow);
        }
    }
}
