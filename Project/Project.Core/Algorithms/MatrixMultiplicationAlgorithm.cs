﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class MatrixMultiplicationAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.matrixData;

        int[,] Multiplication(int[,] a, int[,] b)
        {
            if (a.GetLength(1) != b.GetLength(0))
            {
                throw new Exception("Количество столбцов 1 матрицы должно ровныться количеству столбцов 2 матрицы");
            }

            int[,] r = new int[a.GetLength(0), b.GetLength(1)];

            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    for (int k = 0; k < b.GetLength(0); k++)
                    {
                        r[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return r;
        }

        public void Execute(FileReader fileReader)
        {
            var fileWriter = new FileWriter(AlgorithmPath);
            fileWriter.GenerateFile();

            var matrices = fileReader.ReadFileMatrix();

            for (int i = 0; i < matrices.Count; i+=2)
            {
                if (i != 0 && i % ((int)Math.Sqrt(matrices.Count / 2) * 2) == 0)
                    AlgorithmWorker.text.Append('\n');

                AlgorithmWorker.MeasureExecutionTime(matrices[i], matrices[i+1], Multiplication);
            }

            File.WriteAllText(AlgorithmWorker.path, AlgorithmWorker.text.ToString());
        }
    }
}
