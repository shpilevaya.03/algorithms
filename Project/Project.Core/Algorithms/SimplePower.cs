﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class SimplePower : IAlgorithm
    {
        public DataPath? AlgorithmPath => null;

        int SimplePow(int num, int pow)
        {
            var res = 1;
            var count = 0;
            var counter = 0;

            while (count < pow)
            {
                res *= num;
                count += 1;
                counter += 2;
            }

            return counter;
        }

        public void Execute(FileReader fileReader)
        {
            AlgorithmWorker.CountSteps(SimplePow);
        }
    }
}
