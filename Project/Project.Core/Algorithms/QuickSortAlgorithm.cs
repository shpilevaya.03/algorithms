﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class QuickSortAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        static private Random rand = new Random();
        public static List<int> Quicksort(List<int> elements)
        {
            if (elements.Count() < 2) return elements;
            var pivot = rand.Next(elements.Count());
            var val = elements[pivot];
            var lesser = new List<int>();
            var greater = new List<int>();
            for (int i = 0; i < elements.Count(); i++)
            {
                if (i != pivot)
                {
                    if (elements[i].CompareTo(val) < 0)
                    {
                        lesser.Add(elements[i]);
                    }
                    else
                    {
                        greater.Add(elements[i]);
                    }
                }
            }

            var merged = Quicksort(lesser);
            merged.Add(val);
            merged.AddRange(Quicksort(greater));
            return merged;
        }

        public void Execute(FileReader fileReader)
        {
            var arr = fileReader.ReadFile();

            AlgorithmWorker.RepeatMeasure(arr, Quicksort, 1000, 100);
        }
    }
}
