﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class ConstantAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        int Const(List<int> arr)
        {
            return arr.Count;
        }

        public void Execute(FileReader fileReader)
        {
            var arr = fileReader.ReadFile();

            AlgorithmWorker.RepeatMeasure(arr, Const, 1000, 100);
        }
    }
}
