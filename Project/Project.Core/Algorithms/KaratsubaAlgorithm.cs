﻿using System;
using System.Numerics;

namespace Project.Core.Algorithms
{
    public class KaratsubaAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.longData;

        BigInteger StartAlgorithm(params BigInteger[] integers)
        {
            var res = Karatsuba(integers[0], integers[1]);
            return res;
        }

        BigInteger Karatsuba(BigInteger left, BigInteger right)
        {
            int length = (int)BigInteger.Log10(BigInteger.Abs(right)) + 1;

            if (length < 3)
                return left * right;
            else
            {
                double k = Math.Ceiling(length / 2.0);
                (BigInteger a, BigInteger b) x;
                (BigInteger c, BigInteger d) y;
                (BigInteger p, BigInteger q) sum;
                BigInteger[] muls = new BigInteger[3];

                x.a = left / (BigInteger)Math.Pow(10, k);
                x.b = left % (BigInteger)Math.Pow(10, k);

                y.c = right / (BigInteger)Math.Pow(10, k);
                y.d = right % (BigInteger)Math.Pow(10, k);

                sum.p = x.a + x.b;
                sum.q = y.d + y.c;

                muls[0] = Karatsuba(x.a, y.c);
                muls[1] = Karatsuba(x.b, y.d);
                muls[2] = Karatsuba(sum.p, sum.q);

                BigInteger z = muls[2] - muls[0] - muls[1];

                length = length % 2 == 0 ? length : length + 1;
                var res = (BigInteger)Math.Pow(10, length) * muls[0]
                    + (BigInteger)Math.Pow(10, k) * z
                    + muls[1];
                return res;
            }    
        }

        public void Execute(FileReader fileReader)
        {
            //FileWriter fileWriter = new FileWriter(AlgorithmPath);
            //fileWriter.GenerateFile(2, 150, 1);

            var info = fileReader.ReadFileLongData();

            for (int i = 0; i < info.Count; i++)
            {
                var data = info[i];
                Console.WriteLine($"Длина входных данных: {(int)BigInteger.Log10(BigInteger.Abs(data[0])) + 1}");
                AlgorithmWorker.text.Append(((int)BigInteger.Log10(BigInteger.Abs(data[0])) + 1) + " ");
                AlgorithmWorker.MeasureExecutionTime(data,  StartAlgorithm);
                AlgorithmWorker.text.Append("\n");
            }
            File.WriteAllText(AlgorithmWorker.path, AlgorithmWorker.text.ToString());
            AlgorithmWorker.text.Clear();
        }
    }
}