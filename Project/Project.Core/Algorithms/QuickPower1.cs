﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class QuickPower1 : IAlgorithm
    {
        public DataPath? AlgorithmPath => null;

        int QuickPow1(int num, int pow)
        {
            var c = num;
            var res = 1;
            var k = pow;
            var counter = 0;

            do
            {
                if (k != 0)
                    if (k % 2 == 0)
                    {
                        c = c * c;
                        k = k / 2;
                        counter += 2;
                    }
                    else
                    {
                        res = res * c;
                        k = k - 1;
                        counter += 2;
                    }
                else
                    break;
            }
            while (true);

            return counter;
        }

        public void Execute(FileReader fileReader)
        {
            AlgorithmWorker.CountSteps(QuickPow1);
        }
    }
}
