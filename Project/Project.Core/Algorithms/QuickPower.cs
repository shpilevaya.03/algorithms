﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class QuickPower : IAlgorithm
    {
        public DataPath? AlgorithmPath => null;

        int QuickPow(int num, int pow)
        {
            var c = num;
            var k = pow;
            int res;
            int counter = 0;

            if (k % 2 == 1)
                res = c;
            else
                res = 1;

            do
            {
                k = k / 2;
                c = c * c;
                counter += 2;
                if (k % 2 == 1)
                {
                    res = res * c;
                    counter++;
                }
            }
            while (k != 0);

            return counter;
        }

        public void Execute(FileReader fileReader)
        {
            AlgorithmWorker.CountSteps(QuickPow);
        }
    }
}
