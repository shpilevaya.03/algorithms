﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class GCDAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        int GCD(int a, int b)
        {
            if (a == 0)
                return b;
            return GCD(b % a, a);
        }

        int GcdOfArray(List<int> arr, int idx)
        {
            if (idx == arr.Count - 1)
            {
                return arr[idx];
            }
            int a = arr[idx];
            int b = GcdOfArray(arr, idx + 1);
            return GCD(a, b);
        }

        int DefineEverything(List<int> arr)
        {
            int idx = 0;
            return GcdOfArray(arr, idx);
        }

        public void Execute(FileReader fileReader)
        {
            var arr = fileReader.ReadFile();

            AlgorithmWorker.RepeatMeasure(arr, DefineEverything, 100, 100);
        }
    }
}
