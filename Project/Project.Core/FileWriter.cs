﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Project.Core
{
    public class FileWriter : FileWorker
    {
        protected override DataPath Path { get; set; }

        public FileWriter(DataPath? path = DataPath.data)
        {
            if (path == null) return;
            ChangePath((DataPath)path);
        }

        /// <summary>Генерация файла с длинными числами</summary>
        /// <param name="count">Количество чисел одной длины</param>
        /// <param name="stepCount">Количество шагов</param>
        /// <param name="step">Шаг увеличения количества символов</param>
        public void GenerateFile(int count, int stepCount, int step = 10)
        {
            var res = GenerationData.GenerateLongData(count, stepCount, step);
            string[] text = new string[stepCount];

            for (int i = 0; i < text.Length; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    text[i] += res[i][j] + " ";
                }
            }

            File.WriteAllLines(
                FullPath, text
                );
        }

        /// <summary>Генерация файла с большим количеством чисел</summary>
        /// <param name="count">Количество чисел</param>
        public void GenerateFile(int count) 
        {
            File.WriteAllLines(
                FullPath,
                Array.ConvertAll(GenerationData.GenerateData(count).ToArray(),
                x => x.ToString())
                );
        }

        /// <summary>Генерация файла с матрицами</summary>
        public void GenerateFile()
        {
            List<int[,]> matrices = GenerationData.GenerateMatrices(100);
            List<string> lines = new List<string>();
            string message = "";

            foreach (var matrix in matrices)
            {
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    message = "";
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        message += matrix[i, j].ToString() + " ";
                    }
                    lines.Add(message);
                }
                lines.Add("!");
            }
            File.WriteAllLines(FullPath, lines);
        }
    }
}