﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Project.Core
{
    public class FileReader : FileWorker
    {
        protected override DataPath Path { get; set; }

        public FileReader(DataPath? path = DataPath.data)
        {
            if (path == null) return;
            ChangePath((DataPath)path);
        }

        /// <summary>Чтение файла с данными</summary>
        /// <returns>Возвращает список чисел</returns>
        public List<int> ReadFile()
        {
            var text = File.ReadAllLines(FullPath);
            return Array.ConvertAll(text, x => int.Parse(x)).ToList();
        }

        /// <summary>Чтение файла с длинными данными</summary>
        /// <returns>Возвращает список массивов с числами</returns>
        public List<BigInteger[]> ReadFileLongData()
        {
            var text = File.ReadAllLines(FullPath);
            return Array.ConvertAll(text, x => Array.ConvertAll(x.Split(' ', StringSplitOptions.RemoveEmptyEntries),
                num => BigInteger.Parse(num))).ToList();
        }

        public List<int[,]> ReadFileMatrix()
        {
            var text = File.ReadAllLines(FullPath);

            var matrices = new List<int[,]>();
            List<int> line = new List<int>();
            List<List<int>> matrix = new List<List<int>>();

            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] != "!")
                {
                    line = Array.ConvertAll(text[i].Split(' ', StringSplitOptions.RemoveEmptyEntries),
                        num => int.Parse(num)).ToList();

                    matrix.Add(line);
                }
                else
                {
                    int[,] localMatrix = new int[matrix.Count, matrix[0].Count];

                    for (int j = 0; j < localMatrix.GetLength(0); j++)
                        for (int k = 0; k < localMatrix.GetLength(1); k++)
                        {
                            localMatrix[j, k] = matrix[j][k];
                        }

                    matrices.Add(localMatrix);
                    matrix.Clear();
                }
            }

            return matrices;
        }
    }
}
